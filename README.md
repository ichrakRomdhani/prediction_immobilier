+ Cette répertoire contient les données des immobliers et un code python qui implémente des modèles à des fins de prédictions.
Le but est de prédir les prix des immobiliers à partir de leurs localisation, surface, nombre de pièces et la distance auprés de la mer.

+ Pour cela on a essayé tout d'abord de comprendre la distrubtion des données en utilisant des plots, et ensuite on a extrait des relations entre les attributs(localisation, surface, nombre de pièces,prix...)

+ Pour la modélisation, j'ai tenu en compte que les données sont labélisées et discontinues alors j'ai utilisé les algorithmes de supervised learning comme la regression linéaire multiple et XGBOOST regressor.

+ Les résultats  ne sont pas assez fortes, cependant on peut les améliorer.

